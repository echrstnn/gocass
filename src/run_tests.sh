#!/bin/bash

for i in `ls ../tests/bad/*.go`
do
	./pgoc --type-only $i > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
        echo "[!] Test $i FAILED"
    else
        echo "[ ] Test $i passed"
    fi
done

for i in `ls ../tests/good/*.go`
do
    REF_OUT=$(go run $i)
    SNAME=../tests/good/$(basename $i .go).s
    ./pgoc $i && gcc -no-pie $SNAME -o test && rm $SNAME
    if [ $? -ne 0 ]
    then
        echo "[!] Test $i FAILED"
    else
	    CMP_OUT=$(./test)
        if [ "$REF_OUT" = "$CMP_OUT" ]
        then
            echo "[ ] Test $i passed"
        else
            echo -e "Compiled program output:\n-----\n$CMP_OUT\n-----"
            echo -e "Reference output:\n-----\n$REF_OUT\n-----"
            echo "[!] Test $i FAILED"
        fi
    fi
done
