
open Lib
open Format
open Ast
open Tast

let binop = function
  | Ast.Badd -> "+"
  | Bsub -> "-"
  | Bmul -> "*"
  | Bdiv -> "/"
  | Bmod -> "%"
  | Beq -> "=="
  | Bne -> "!="
  | Blt -> "<"
  | Ble -> "<="
  | Bgt -> ">"
  | Bge -> ">="
  | Band -> "&&"
  | Bor -> "||"
let unop = function
  | Ast.Uneg -> "-"
  | Unot -> "!"
  | Uamp -> "&"
  | Ustar -> "*"

(* TODO afficher ast non type *)

let current_prefix = ref ""

let _treeprint_internal fmt f arg suffix =
  let save = !current_prefix in
  (current_prefix := !current_prefix ^ suffix;
  fprintf fmt "\n%s   |__\n%s   " save !current_prefix;
  f fmt arg;
  current_prefix := save)

let treeprint fmt f arg = _treeprint_internal fmt f arg "   |"

let treeprint_last fmt f arg = _treeprint_internal fmt f arg "    "

let rec treeprint_list fmt f l = match l with
  | [] -> ()
  | x::q -> treeprint fmt f x; treeprint_list fmt f q

let rec treeprint_lastlist fmt f l = match l with
  | [] -> ()
  | [x] -> treeprint_last fmt f x
  | x::q -> treeprint fmt f x; treeprint_lastlist fmt f q


let ident fmt i = fprintf fmt "ident: %s" i.id

let rec ptyp fmt pt = match pt with
  | PTident i -> fprintf fmt "PTypident"; treeprint_last fmt ident i
  | PTptr ptr -> fprintf fmt "PTypptr"; treeprint_last fmt ptyp ptr

let rec pexpr fmt pe = match pe.pexpr_desc with
  | PEskip -> fprintf fmt "PEskip"
  | PEconstant (Cbool c) -> fprintf fmt "PEconstant: %B" c
  | PEconstant (Cint c) -> fprintf fmt "PEconstant: %Ld" c
  | PEconstant (Cstring c) -> fprintf fmt "PEconstant: %S" c
  | PEbinop (op, e1, e2) -> fprintf fmt "PEbinop %s" (binop op); treeprint fmt pexpr e1; treeprint_last fmt pexpr e2
  | PEunop (op, e) -> fprintf fmt "PEunop %s" (unop op); treeprint_last fmt pexpr e
  | PEnil -> fprintf fmt "PEnil"
  | PEcall (i, pel) -> fprintf fmt "PEcall"; treeprint fmt ident i; treeprint_lastlist fmt pexpr pel
  | PEident i -> fprintf fmt "PEident"; treeprint_last fmt ident i
  | PEdot (pe, i) -> fprintf fmt "PEdot"; treeprint fmt pexpr pe; treeprint_last fmt ident i
  | PEassign (lhs, rhs) -> fprintf fmt "PEassign"; treeprint_list fmt pexpr lhs; treeprint_lastlist fmt pexpr rhs
  | PEvars (il, None, pel) -> fprintf fmt "PEvars"; treeprint_list fmt ident il; treeprint_lastlist fmt pexpr pel
  | PEvars (il, Some pt, pel) -> fprintf fmt "PEvars"; treeprint_list fmt ident il; treeprint fmt ptyp pt; treeprint_lastlist fmt pexpr pel
  | PEif (c, t, e) -> fprintf fmt "PEif"; treeprint_lastlist fmt pexpr [c; t; e]
  | PEreturn pel -> fprintf fmt "PEreturn"; treeprint_lastlist fmt pexpr pel
  | PEblock pel -> fprintf fmt "PEblock"; treeprint_lastlist fmt pexpr pel
  | PEfor (e1, e2) -> fprintf fmt "PEfor"; treeprint_lastlist fmt pexpr [e1; e2]
  | PEincdec (pe, Inc) -> fprintf fmt "PEinc"; treeprint_last fmt pexpr pe
  | PEincdec (pe, Dec) -> fprintf fmt "PEdec"; treeprint_last fmt pexpr pe

let pparam fmt (i, pt) =
  fprintf fmt "pparam";
  treeprint fmt ident i;
  treeprint_last fmt ptyp pt

let pfield fmt (i, pt) =
  fprintf fmt "pfield";
  treeprint fmt ident i;
  treeprint_last fmt ptyp pt

let pdecl fmt pd = match pd with
  | PDfunction pf ->
      fprintf fmt "PDfunction";
      treeprint fmt ident pf.pf_name;
      treeprint_list fmt pparam pf.pf_params;
      treeprint_list fmt ptyp pf.pf_typ;
      treeprint_last fmt pexpr pf.pf_body
  | PDstruct ps ->
      fprintf fmt "PDstruct";
      treeprint fmt ident ps.ps_name;
      treeprint_lastlist fmt pfield ps.ps_fields


let ast_file fmt (imp, dl) =
  fprintf fmt "---------@\n";
  treeprint fmt (fun fmt imp -> fprintf fmt "import_fmt: %B" imp) imp;
  treeprint_lastlist fmt pdecl dl;
  fprintf fmt "@\n---------@\n"

let rec typ fmt = function
  | Tint -> fprintf fmt "int"
  | Tbool -> fprintf fmt "bool"
  | Tstring -> fprintf fmt "string"
  | Tstruct s -> fprintf fmt "%s" s.s_name
  | Tptr ty -> fprintf fmt "*%a" typ ty
  (* | Tmany [] -> fprintf fmt "" *)
  | Tmany l -> print_list comma typ fmt l
  | Tnil -> fprintf fmt "nil"

let typl fmt l = print_list comma typ fmt l

let rec expr fmt e =
  let ptp () = fprintf fmt " <%a>" typ e.expr_typ in
  match e.expr_desc with
  | TEskip -> fprintf fmt "TEskip"; ptp ()
  | TEnil -> fprintf fmt "TEnil"; ptp ()
  | TEconstant (Cint n) -> fprintf fmt "TEconstant %Ld" n; ptp ()
  | TEconstant (Cbool b) -> fprintf fmt "TEconstant %b" b; ptp ()
  | TEconstant (Cstring s) -> fprintf fmt "TEconstant %S" s; ptp ()
  | TEbinop (op, e1, e2) -> fprintf fmt "TEbinop %s" (binop op); ptp (); treeprint fmt expr e1; treeprint_last fmt expr e2
  | TEunop (op, e1) -> fprintf fmt "TEunop %s" (unop op); ptp (); treeprint_last fmt expr e1
  | TEnew ty -> fprintf fmt "TEnew %a" typ ty; ptp ()
  | TEcall (f, el) -> fprintf fmt "TEcall %s" f.fn_name; ptp (); treeprint_lastlist fmt expr el
  | TEident v -> fprintf fmt "TEident (%a)" var v; ptp ()
  | TEdot (e1, f) -> fprintf fmt "TEdot .%s" f.f_name; ptp (); treeprint_last fmt expr e1
  | TEassign (lvl, el) -> fprintf fmt "TEassign %d variables" (List.length lvl); ptp (); treeprint_list fmt expr lvl; treeprint_lastlist fmt expr el
  | TEif (e1, e2, e3) -> fprintf fmt "TEif"; ptp (); treeprint fmt expr e1; treeprint fmt expr e2; treeprint_last fmt expr e3
  | TEreturn el -> fprintf fmt "TEreturn"; ptp (); treeprint_lastlist fmt expr el
  | TEblock bl -> fprintf fmt "TEblock"; ptp (); treeprint_lastlist fmt expr bl
  | TEfor (e1, e2) -> fprintf fmt "TEfor"; ptp (); treeprint fmt expr e1; treeprint_last fmt expr e2
  | TEprint el -> fprintf fmt "TEprint"; ptp (); treeprint_lastlist fmt expr el
  | TEincdec (e1, op) -> fprintf fmt "TEincdec %s" (match op with Inc -> "++" | Dec -> "--"); ptp (); treeprint_last fmt expr e1
  | TEvars vl -> fprintf fmt "TEvars"; ptp (); treeprint_lastlist fmt var vl

and var fmt v = fprintf fmt "var %s with id %d <%a>" v.v_name v.v_id typ v.v_typ

let field fmt f = fprintf fmt "field %s <%a>" f.f_name typ f.f_typ

let decl fmt = function
  | TDfunction (f, e) -> fprintf fmt "TDfunction %s <%a>" f.fn_name typl f.fn_typ; treeprint_list fmt var f.fn_params; treeprint_last fmt expr e
  | TDstruct s -> fprintf fmt "TDstruct %s" s.s_name; treeprint_lastlist fmt field (List.of_seq (Hashtbl.to_seq_values s.s_fields))

let file fmt dl =
  fprintf fmt "---------@\n";
  treeprint_lastlist fmt decl dl;
  fprintf fmt "@\n---------@\n"

