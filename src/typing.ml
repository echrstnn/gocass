
open Format
open Lib
open Ast
open Tast

let debug = ref false

let dummy_loc = Lexing.dummy_pos, Lexing.dummy_pos

exception Error of Ast.location * string

let error loc e = raise (Error (loc, e))


(* Teste la compatibilite entre deux types comme defini dans la semantique *)
let rec eq_type ty1 ty2 = match ty1, ty2 with
  | Tint, Tint | Tbool, Tbool | Tstring, Tstring -> true
  | Tstruct s1, Tstruct s2 -> s1 == s2
  | Tptr ty1, Tptr ty2 -> eq_type ty1 ty2
  | Tptr _, Tnil | Tnil, Tptr _ -> true
  | _ -> false

let fmt_used = ref false
let fmt_imported = ref false

let evar v = { expr_desc = TEident v; expr_typ = v.v_typ }

let new_var =
  let id = ref 0 in
  fun x loc ?(used=false) ty ->
    incr id;
    { v_name = x; v_id = !id; v_loc = loc; v_typ = ty; v_used = used; v_addr = false; v_ofs = 0 }

module Env = struct
  module M = Map.Make(String)
  type t = var M.t
  let empty = M.empty
  let find = M.find
  let add env name v = M.add name v env
  let mem env name = M.mem env name

  let all_vars = ref []
  let all_structs = ref empty
  let all_functions = ref empty

  let check_unused () =
    let check v =
      if v.v_name <> "_" && not v.v_used then error v.v_loc (v.v_name ^ " declared but not used") in
    List.iter check !all_vars

  let var x loc ?used ty env =
    let v = new_var x loc ?used ty in
    all_vars := v :: !all_vars;
    add env v.v_name v, v

end

let rec type_type = function
  | PTident { id = "int" } -> Tint
  | PTident { id = "bool" } -> Tbool
  | PTident { id = "string" } -> Tstring
  | PTident { id = id; loc = loc } ->
      if not (Env.mem id !Env.all_structs) then
        error loc ("undefined: " ^ id);
      Tstruct (Env.find id !Env.all_structs)
  | PTptr ty -> Tptr (type_type ty)

let tvoid = Tmany []
let make d ty = { expr_desc = d; expr_typ = ty }
let stmt d = make d tvoid

let rec expr env rettyp e =
 let e, ty, rt, lvalue = expr_desc env rettyp e.pexpr_loc e.pexpr_desc in
  { expr_desc = e; expr_typ = ty }, rt, lvalue

(* Fonction generique pour tous les cas ou on peut accepter une liste d'arguments (Tmany) ou un argument seul
   Sert a gerer la semantique de fmt.Print, des fonctions a plusieurs parametres, des affectations multiples... en un seul endroit du code *)
and exprl_typl env rettyp loc el =
  let tel = List.map (fun e -> let te, _, _ = expr env rettyp e in te) el in
  match tel with
    | [{expr_typ=Tmany []}] -> error loc "void cannot be used like other types"
    | [{expr_typ=Tmany tpl}] -> tel, tpl
    | _ ->
        let checktype = (function
          | {expr_typ=(Tmany [])} -> error loc "void cannot be used like other types"
          | {expr_typ=(Tmany _ )} -> error loc "tuples cannot be used like other types"
          | _ -> ()) in
        List.iter checktype tel;
        tel, List.map (fun e -> e.expr_typ) tel

and expr_desc env rettyp loc = function
  | PEskip ->
     TEskip, tvoid, false, false
  | PEconstant c ->
      let typ = (match c with | Cbool _ -> Tbool | Cint _ -> Tint | Cstring _ -> Tstring) in
      TEconstant c, typ, false, false

  (* Typage des operations binaires *)
  | PEbinop (Badd | Bsub | Bmul | Bdiv | Bmod as op, e1, e2) ->
      let te1, _, _ = expr env rettyp e1 and te2, _, _ = expr env rettyp e2 in
      if not (eq_type te1.expr_typ Tint) then error e1.pexpr_loc "this expression should have type int";
      if not (eq_type te2.expr_typ Tint) then error e2.pexpr_loc "this expression should have type int";
      TEbinop (op, te1, te2), Tint, false, false
  | PEbinop (Beq | Bne as op, e1, e2) ->
      let te1, _, _ = expr env rettyp e1 and te2, _, _ = expr env rettyp e2 in
      if not (eq_type te1.expr_typ te2.expr_typ) then error loc "both sides should have same type";
      if (te1.expr_typ = Tnil) && (te2.expr_typ = Tnil) then error loc "both sides should not be nil at the same time";
      TEbinop (op, te1, te2), Tbool, false, false
  | PEbinop (Blt | Ble | Bgt | Bge as op, e1, e2) ->
      let te1, _, _ = expr env rettyp e1 and te2, _, _ = expr env rettyp e2 in
      if not (eq_type te1.expr_typ Tint) then error e1.pexpr_loc "this expression should have type int";
      if not (eq_type te2.expr_typ Tint) then error e2.pexpr_loc "this expression should have type int";
      TEbinop (op, te1, te2), Tbool, false, false
  | PEbinop (Band | Bor as op, e1, e2) ->
      let te1, _, _ = expr env rettyp e1 and te2, _, _ = expr env rettyp e2 in
      if not (eq_type te1.expr_typ Tbool) then error e1.pexpr_loc "this expression should have type bool";
      if not (eq_type te2.expr_typ Tbool) then error e2.pexpr_loc "this expression should have type bool";
      TEbinop (op, te1, te2), Tbool, false, false

  (* Typage des operations unaires *)
  | PEunop (Uneg, e1) ->
      let te1, _, _ = expr env rettyp e1 in
      if not (eq_type te1.expr_typ Tint) then error e1.pexpr_loc "this expression should have type int";
      TEunop (Uneg, te1), Tint, false, false
  | PEunop (Unot, e1) ->
      let te1, _, _ = expr env rettyp e1 in
      if not (eq_type te1.expr_typ Tbool) then error e1.pexpr_loc "this expression should have type bool";
      TEunop (Unot, te1), Tbool, false, false
  | PEunop (Uamp, e1) ->
      let te1, _, lvalue1 = expr env rettyp e1 in
      if not lvalue1 then error e1.pexpr_loc "this expression is not an l-value";
      TEunop (Uamp, te1), Tptr te1.expr_typ, false, false
  | PEunop (Ustar, e1) ->
      let te1, _, _ = expr env rettyp e1 in
      if te1.expr_typ = Tnil then error loc "cannot dereference nil";
      let typ = (match te1.expr_typ with | Tptr t -> t | _ -> error e1.pexpr_loc "this expression cannot be dereferenced") in
      TEunop (Ustar, te1), typ, false, true

  (* Typages des appels de fonctions, fmt.Print et new *)
  | PEcall ({id = "fmt.Print"}, el) ->
      if not !fmt_imported then error loc "fmt used but not imported";
      fmt_used := true;
      let exprl, _ = exprl_typl env rettyp loc el in
      TEprint (exprl), tvoid, false, false
  | PEcall ({id="new"}, [{pexpr_desc=PEident id}]) ->
      let ty = type_type (PTident id) in
      TEnew ty, Tptr ty, false, false
  | PEcall ({id="new"}, _) ->
      error loc "new expects a type"
  | PEcall (id, el) ->
      (try
        let foundfunc = Env.find id.id !Env.all_functions in
        let exprl, typl = exprl_typl env rettyp loc el in (* on type les arguments *)
        if List.length foundfunc.fn_params <> List.length typl then error loc ("function " ^ foundfunc.fn_name ^ " called with " ^ (string_of_int (List.length typl)) ^ " parameters instead of " ^ (string_of_int (List.length foundfunc.fn_params)));
        List.iter2 (fun var tp ->
          if not (eq_type var.v_typ tp) then error loc ("parameter " ^ var.v_name ^ " of function " ^ foundfunc.fn_name ^ " has not the expected type")
        ) foundfunc.fn_params typl;
        if List.length foundfunc.fn_typ = 1 then
          TEcall (foundfunc, exprl), List.hd foundfunc.fn_typ, false, false (* si la fonction renvoie une seule valeur *)
        else
          TEcall (foundfunc, exprl), Tmany foundfunc.fn_typ, false, false (* 0 ou plusieurs valeurs *)
      with Not_found -> error id.loc ("unbound function " ^ id.id))
    
  (* Typage des structures de controle *)
  | PEfor (e, b) ->
      let te, _, _ = expr env rettyp e and tb, _, _ = expr env rettyp b in
      if not (eq_type te.expr_typ Tbool) then error e.pexpr_loc "this expression should have type bool";
      TEfor (te, tb), tvoid, false, false
  | PEif (e1, e2, e3) ->
      let te1, _, _ = expr env rettyp e1 and te2, ret2, _ = expr env rettyp e2 and te3, ret3, _ = expr env rettyp e3 in
      if not (eq_type te1.expr_typ Tbool) then error e1.pexpr_loc "this expression should have type bool";
      TEif (te1, te2, te3), tvoid, ret2 && ret3, false
  
  (* Typage des identifiants *)
  | PEnil ->
      TEnil, Tnil, false, false
  | PEident {id=id} ->
      if id = "_" then error loc "unbound variable _";
      (try
        let v = Env.find id env in
        v.v_used <- true;
        TEident v, v.v_typ, false, true
      with Not_found -> error loc ("unbound variable " ^ id))
    
  (* Typage des champs de structure *)
  | PEdot (e, id) ->
      let te, _, lv = expr env rettyp e in
      (match te.expr_typ with
        | Tstruct s -> (* s.champ *)
            (try
              let f = Hashtbl.find s.s_fields id.id in
              TEdot (te, f), f.f_typ, false, lv
            with Not_found -> error id.loc ("field " ^ id.id ^ " is not in structure " ^ s.s_name))
        | Tptr (Tstruct s) -> (* (&s).champ *)
            (try
              let f = Hashtbl.find s.s_fields id.id in
              let star_s = make (TEunop (Ustar, te)) (Tstruct s) in
              TEdot (star_s, f), f.f_typ, false, true
            with Not_found -> error id.loc ("field " ^ id.id ^ " is not in structure " ^ s.s_name))
        | Tptr Tnil -> (* nil.champ *)
            error e.pexpr_loc "cannot dereference nil"
        | _ ->
            error e.pexpr_loc "this expression should have type structure or structure pointer")
          
  (* Typage des affectations *)
  | PEassign (lvl, el) ->
      let tlvl = List.map (fun e -> (* on va typer les membres a gauche *)
        match e.pexpr_desc with
          | PEident {id=id} -> (* le ieme membre est un identifiant *)
              (try
                let v = Env.find id env in
                make (TEident v) v.v_typ
              with Not_found -> error loc ("unbound variable " ^ id))
          | _ -> (* le ieme membre n'est pas un identifiant, ex: s.champ = ... *)
            let te, _, lv = expr env rettyp e in
            if not lv then error e.pexpr_loc "this expression should be a l-value";
            te
      ) lvl in
      let exprl, typl = exprl_typl env rettyp loc el in (* on type les membres a droite *)
      if List.length tlvl <> List.length typl then error loc ("cannot assign " ^ (string_of_int (List.length tlvl)) ^ " variables from " ^ (string_of_int (List.length typl)) ^ " values");
      List.iter2 (fun lv typ ->
        if not (eq_type lv.expr_typ typ) then error loc "right handside has not the same type as the left one"
      ) tlvl typl;
      TEassign (tlvl, exprl), tvoid, false, false
  (* Typage return *)
  | PEreturn el ->
      let exprl, typl = exprl_typl env rettyp loc el in
      if List.length typl <> List.length rettyp then error loc ("this return statement should have " ^ (string_of_int (List.length rettyp)) ^ " arguments instead of " ^ (string_of_int (List.length typl)));
      if List.exists2 (fun a b -> not (eq_type a b)) typl rettyp then error loc "one return value has not the expected type";
      TEreturn exprl, tvoid, true, false
  
  (* Typage d'un bloc *)
  | PEblock el ->
      let ret = ref false in
      let newenv = ref env and block_vars = ref [] in
      let tel = ref [] in
      let type_block_expr e = (match e with
        | {pexpr_desc=PEvars (idl, pto, pel); pexpr_loc=loc} -> (* Declaration de variable *)
            let vars = ref [] in
            let addenv typ id =
              let newenv_, v = Env.var id.id id.loc typ !newenv in
              newenv := newenv_;
              vars := v::!vars;
              if v.v_name <> "_" && List.mem v.v_name !block_vars then error loc ("multiple definition of variable " ^ v.v_name);
              block_vars := v.v_name::!block_vars in
            let exprl, typl = exprl_typl !newenv rettyp loc pel in
            if typl <> [] && List.length typl <> List.length idl then
              error loc ("cannot assign " ^ (string_of_int (List.length idl)) ^ " variables from " ^ (string_of_int (List.length typl)) ^ " values");
            (match pto with
              | None -> (* x := ... *)
                  if exprl = [] then
                    error loc "type should be specified"
                  else (
                    if List.mem Tnil typl then error loc "cannot deduce variable type from nil expression";
                    List.iter2 addenv typl idl)
              | Some pt -> (* var x ... = ... *)
                  let typ = type_type pt in
                  if exprl <> [] then
                    List.iter2 (fun id e ->
                      if not (eq_type e.expr_typ typ) then error id.loc ("this expression has not the same type as variable " ^ id.id)
                    ) idl exprl;
                  List.iter (addenv typ) idl
            );
            tel := (stmt (TEvars !vars))::!tel;
            if exprl <> [] then
              let lhs = List.map (fun v -> make (TEident v) v.v_typ) (List.rev !vars) in
              tel := (stmt (TEassign (lhs, exprl)))::!tel
        | e -> (* Toute expression autre qu'une declaration de variables *)
            let te, ret_, _ = expr !newenv rettyp e in
            if ret_ then ret := true;
            tel := te::!tel) in
      List.iter type_block_expr el;
      TEblock (List.rev !tel), tvoid, !ret, false
    
  (* Typage incrementation et decrementation *)
  | PEincdec (e, op) ->
      let te, _, lvalue = expr env rettyp e in
      if not (eq_type te.expr_typ Tint) then error e.pexpr_loc "this expression should have type int";
      if not lvalue then error e.pexpr_loc "this expression should be a l-value";
      TEincdec (te, op), tvoid, false, false
    
  (* Cas exclus par le typage du bloc *)
  | PEvars _ ->
      assert false 

let found_main = ref false

(* 1. declare structures *)
let phase1 = function
  | PDstruct { ps_name = ps_name } ->
      if Env.mem ps_name.id !Env.all_structs then
        error ps_name.loc ("multiple definition of struct " ^ ps_name.id)
      else
        Env.all_structs := (Env.add !Env.all_structs ps_name.id { s_name = ps_name.id; s_fields = Hashtbl.create 5; s_sz = -1 })
  | PDfunction _ -> ()

let rec sizeof = function
  | Tint | Tbool | Tstring | Tptr _ -> 8
  | Tstruct s ->
      if s.s_sz = -1 then
        s.s_sz <- List.fold_left (fun ofs f ->
          f.f_ofs <- ofs;
          ofs + sizeof f.f_typ
        ) 0 (List.of_seq (Hashtbl.to_seq_values s.s_fields));
      s.s_sz
  | _ -> assert false 

(* 2. declare functions and type fields *)
let phase2 = function
  | PDfunction { pf_name = pf_name; pf_params = pf_params; pf_typ = pf_typ } ->
      if Env.mem pf_name.id !Env.all_functions then
        error pf_name.loc ("multiple definition of function " ^ pf_name.id);
      let paramsl = ref [] in
      let func_params (id, pt) =
        if List.exists (fun x -> x.v_name = id.id) !paramsl then
          error id.loc ("multiple definition of function parameter " ^ id.id);
        paramsl := (new_var id.id id.loc (type_type pt))::!paramsl in
      List.iter func_params pf_params;
      let typl = List.map type_type pf_typ in
      if pf_name.id = "main" then
        (found_main := true;
        if typl <> [] then error pf_name.loc "function main should not have any return value";
        if pf_params <> [] then error pf_name.loc "function main should not have any parameters");
      Env.all_functions := (Env.add !Env.all_functions pf_name.id { fn_name = pf_name.id; fn_params = List.rev !paramsl; fn_typ = typl })
  | PDstruct { ps_name = ps_name; ps_fields = ps_fields } ->
      let s = Env.find ps_name.id !Env.all_structs in
      let field i (id, pt) =
        if Hashtbl.mem s.s_fields id.id then
          error id.loc ("multiple definition of struct field " ^ id.id);
        Hashtbl.add s.s_fields id.id { f_name = id.id; f_typ = type_type pt; f_ofs = 0; f_print_order = i } in
      List.iteri field ps_fields

(* 3. type check function bodies *)
let decl = function
  | PDfunction { pf_name = pf_name; pf_body = pf_body } ->
      let foundfunc = Env.find pf_name.id !Env.all_functions in
      let env = ref Env.empty in
      List.iter (fun v -> env := Env.add !env v.v_name v) foundfunc.fn_params;
      let body, ret, _ = expr !env foundfunc.fn_typ pf_body in
      if foundfunc.fn_typ <> [] && not ret then
        error pf_name.loc ("function " ^ pf_name.id ^ " doesn't return anything in some cases");
      TDfunction (foundfunc, body)
  | PDstruct {ps_name={id}} ->
      let s = Env.find id !Env.all_structs in
      TDstruct s

let check_rec_structs () =
  let dans_pile = ref Env.empty in
  let rec dfs s =
    if Env.mem s.s_name !dans_pile then
      (if Env.find s.s_name !dans_pile then
        error dummy_loc ("structure " ^ s.s_name ^ " is recursive"))
    else
      (dans_pile := Env.add !dans_pile s.s_name true;
      Hashtbl.iter (fun _ f -> match f.f_typ with
        | Tstruct s2 -> dfs s2
        | _ -> ()
      ) s.s_fields;
      dans_pile := Env.add !dans_pile s.s_name false) in
  Env.M.iter (fun _ s ->
    if not (Env.mem s.s_name !dans_pile) then dfs s
  ) !Env.all_structs
    
let file ~debug:b (imp, dl) =
  debug := b;
  fmt_imported := imp;
  List.iter phase1 dl;
  List.iter phase2 dl;
  if not !found_main then error dummy_loc "missing method main";
  let dl = List.map decl dl in
  Env.check_unused ();
  check_rec_structs ();
  if imp && not !fmt_used then error dummy_loc "fmt imported but not used";
  dl
