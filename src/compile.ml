(* étiquettes
     F_function      entrée fonction
     E_function      sortie fonction
     L_xxx           sauts
     S_xxx           chaîne

   expression calculée avec la pile si besoin, résultat final dans %rdi

   fonction : arguments sur la pile, résultat dans %rax ou sur la pile

            res k
            ...
            res 1
            arg n
            ...
            arg 1
            adr. retour
   rbp ---> ancien rbp
            ...
            var locales
            ...
            calculs
   rsp ---> ...

*)

open Format
open Ast
open Tast
open X86_64

let debug = ref false

let strings = Hashtbl.create 32
let alloc_string =
  let r = ref 0 in
  fun s ->
    incr r;
    let l = "S_" ^ string_of_int !r in
    Hashtbl.add strings l s;
    l

let sizeof = Typing.sizeof

let alloc n = movq (imm 1) (reg rdi) ++ movq (imm n) (reg rsi) ++ call "calloc"

let new_label =
  let r = ref 0 in fun () -> incr r; "L_" ^ string_of_int !r

type env = {
  exit_label: string;
  next_local_ofs: int (* a quel offset par rapport a rbp stocker la prochaine variable locale *)
}

(* Calcule le champ v_ofs de la variable v dans l'environnement env, renvoie le nouvel environnement *)
let compute_ofs env v =
  v.v_ofs <- env.next_local_ofs - sizeof v.v_typ + 8;
  { exit_label = env.exit_label; next_local_ofs = env.next_local_ofs - sizeof v.v_typ }

let mk_bool d = { expr_desc = d; expr_typ = Tbool }

(* f reçoit le label correspondant à ``renvoyer vrai'' *)
let compile_bool f =
  let l_true = new_label () and l_end = new_label () in
  f l_true ++
  movq (imm 0) (reg rdi) ++ jmp l_end ++
  label l_true ++ movq (imm 1) (reg rdi) ++ label l_end

(* Pour debugger le code ASM *)
let comment_expr e s =
  let  desc = match e.expr_desc with
    | TEbinop (op, _, _) -> "binop " ^ Pretty.binop op
    | TEunop (op, _) -> "unop " ^ Pretty.unop op
    | TEnew _ -> "new"
    | TEcall (f, _) -> "call " ^ f.fn_name
    | TEident v -> "ident " ^ v.v_name
    | TEdot (_, f) -> "dot ." ^ f.f_name
    | TEassign _ -> "assign"
    | TEvars _ -> "vars"
    | TEif _ -> "if"
    | TEreturn _ -> "return"
    | TEfor _ -> "for"
    | TEprint _ -> "print"
    | _ -> "" in
  if desc <> "" && !debug then comment (s ^ " " ^ desc)
  else nop

let rec expr env e =
  comment_expr e "<" ++
  (match e.expr_desc with

  (* Compilation des termes constants *)
  | TEskip ->
      nop
  | TEconstant (Cbool true) ->
      movq (imm 1) (reg rdi)
  | TEconstant (Cbool false) ->
      movq (imm 0) (reg rdi)
  | TEconstant (Cint x) ->
      movq (imm64 x) (reg rdi)
  | TEnil ->
      xorq (reg rdi) (reg rdi)
  | TEconstant (Cstring s) ->
      movq (ilab (alloc_string s)) (reg rdi)
    
  (* Compilation des operations binaires *)
  | TEbinop (Band, e1, e2) ->
      expr env e1 ++ pushq (reg rdi) ++ expr env e2 ++ popq rax ++
      andq (reg rax) (reg rdi)
  | TEbinop (Bor, e1, e2) ->
      expr env e1 ++ pushq (reg rdi) ++ expr env e2 ++ popq rax ++
      orq (reg rax) (reg rdi)
  | TEbinop (Blt | Ble | Bgt | Bge as op, e1, e2) ->  
      expr env e1 ++ pushq (reg rdi) ++ expr env e2 ++ popq rax ++ (* e1 -> rax, e2 -> rdi *)
      movq (reg rdi) (reg rbx) ++
      xorq (reg rdi) (reg rdi) ++
      cmpq (reg rbx) (reg rax) ++
      (match op with
        | Blt -> setl
        | Ble -> setle
        | Bgt -> setg
        | Bge -> setge
        | _ -> assert false
      ) (reg dil)
  | TEbinop (Badd | Bsub | Bmul | Bdiv | Bmod as op, e1, e2) ->
      expr env e1 ++ pushq (reg rdi) ++ expr env e2 ++ popq rax ++ (* e1 -> rax, e2 -> rdi*)
      movq (reg rdi) (reg rbx) ++
      (match op with
        | Badd -> addq  (reg rbx) (reg rax) ++ movq (reg rax) (reg rdi)
        | Bsub -> subq  (reg rbx) (reg rax) ++ movq (reg rax) (reg rdi)
        | Bmul -> imulq (reg rbx) (reg rax) ++ movq (reg rax) (reg rdi)
        | Bdiv -> cqto ++ idivq (reg rbx)   ++ movq (reg rax) (reg rdi)
        | Bmod -> cqto ++ idivq (reg rbx)   ++ movq (reg rdx) (reg rdi)
        | _ -> assert false)
  
  (* Compilation des tests d'egalite *)
  | TEbinop (Beq | Bne as op, e1, e2) ->
      let rsi_if_struct = match e1.expr_typ with | Tstruct _ -> movq (reg rsi) (reg rdi) | _ -> nop in (* met rsi dans rdi si on travaille avec des structures *)
      expr env e1 ++
      rsi_if_struct ++
      pushq (reg rdi) ++
      expr env e2 ++
      popq rax ++ (* e1 -> rax, e2 -> rdi ou rsi si structure *)
      rsi_if_struct ++
      movq (reg rdi) (reg rbx) ++ (* e1 -> rax, e2 -> rbx *)
      (match e1.expr_typ with
        | Tint | Tptr _ | Tnil | Tbool -> (* egalite entre types "simples" *)
            xorq (reg rdi) (reg rdi) ++
            cmpq (reg rax) (reg rbx) ++
            (match op with | Beq -> sete | Bne -> setne | _ -> assert false) (reg dil)
        | Tstring -> (* egalite entre chaines de caracteres *)
            call "eq_string" ++
            if op = Bne then testb (reg dil) (reg dil) ++ sete (reg dil)
            else nop
        | Tstruct s -> (* egalite entre structures *)
            call ("eq_" ^ s.s_name) ++
            if op = Bne then testb (reg dil) (reg dil) ++ sete (reg dil)
            else nop
        | _ -> assert false)

  (* Operation unaires *)
  | TEunop (Uneg, e1) ->
      expr env e1 ++
      xorq (reg rax) (reg rax) ++
      subq (reg rdi) (reg rax) ++
      movq (reg rax) (reg rdi)
  | TEunop (Unot, e1) ->
      expr env e1 ++
      testb (reg dil) (reg dil) ++
      sete (reg dil)
  | TEunop (Uamp, e1) ->
      expr env e1 ++
      movq (reg rsi) (reg rdi) (* vu que rsi donne l'adresse de la lvalue il suffit de faire rsi -> rdi *)
  | TEunop (Ustar, e1) ->
      expr env e1 ++
      movq (reg rdi) (reg rsi) ++ (* rdi -> rsi *)
      movq (ind rsi) (reg rdi)
    
  (* Compilation de l'affichage *)
  | TEprint [] -> nop
  | TEprint el ->
      List.fold_left2 (fun code e nexte ->
        code ++ expr env e ++
        (match e.expr_typ with
          | Tint -> call "print_int"
          | Tbool -> call "print_bool"
          | Tstring -> call "print_string"
          | Tnil -> call "print_nil"
          | Tptr (Tstruct s) ->
              let lnil = new_label () and l = new_label () in
              testq (reg rdi) (reg rdi) ++
              jz lnil ++
              movq (reg rdi) (reg rsi) ++
              pushq (reg rsi) ++
              movq (ilab "S_amp") (reg rdi) ++
              call "print_string" ++
              popq rsi ++
              call ("print_struct_" ^ s.s_name) ++
              jmp l ++
              label lnil ++
              call "print_nil" ++
              label l
          | Tptr _ -> call "print_pointer"
          | Tstruct s -> call ("print_struct_" ^ s.s_name)
          | _ -> assert false) ++
        if e.expr_typ <> Tstring && nexte.expr_typ <> Tstring then call "print_space"
        else nop
      ) nop el (List.tl el @ [Typing.make TEskip Tstring])
  
  (* Compilation des identifiants et affectations *)
  | TEident x ->
      assert (x.v_ofs <> 0);
      leaq (ind ~ofs:x.v_ofs rbp) rsi ++ movq (ind rsi) (reg rdi)
  | TEassign ([lv], [e1]) ->
      expr env lv ++ pushq (reg rsi) ++ expr env e1 ++ popq rax ++
      (match lv.expr_typ with
        | Tstruct s -> movq (reg rsi) (reg rbx) ++ call ("copy_" ^ s.s_name) (* copie de la structure (recursivement) *)
        | _ -> movq (reg rdi) (ind rax))
  (* Cas exclus par rewrite *)
  | TEassign (_, _) ->
      assert false
  
  (* Compilation d'un bloc *)
  | TEblock el ->
      let newenv = ref env in
      let block =
        List.fold_left (fun code e ->
          code ++
          match e.expr_desc with
            | TEvars vl ->
                List.fold_left (fun code v ->
                  newenv := compute_ofs !newenv v;
                  code ++
                  leaq (ind ~ofs:(-sizeof v.v_typ) rsp) rsp (* allouer de l'espace pour v *)
                ) nop vl
            | _ -> expr !newenv e
        ) nop el in
      let sz_here = env.next_local_ofs - !newenv.next_local_ofs in
      block ++
      if sz_here <> 0 then leaq (ind ~ofs:sz_here rsp) rsp (* on desalloue l'espace utilise par les variables de ce bloc *)
      else nop
    
  (* Compilation des structures de controle *)
  | TEif (e1, e2, e3) ->
      let lelse = new_label () and lend = new_label () in
      expr env e1 ++
      testb (reg dil) (reg dil) ++
      jz lelse ++
      expr env e2 ++ (* then *)
      jmp lend ++
      label lelse ++ 
      expr env e3 ++ (* else *)
      label lend
  | TEfor (e1, e2) ->
      let lcond = new_label () and lloop = new_label () in
      jmp lcond ++
      label lloop ++ (* coprs de la boucle *)
      expr env e2 ++
      label lcond ++
      expr env e1 ++
      testb (reg dil) (reg dil) ++ (* while ( .. ) *)
      jnz lloop
    
  (* Compilation new et appels *)
  | TEnew ty ->
      alloc (sizeof ty) ++
      movq (reg rax) (reg rdi)
  | TEcall (f, el) ->
      List.fold_right (fun e code->
        code ++
        expr env e ++
        match e.expr_typ with
          | Tstruct s ->
              pushq (reg rsi) ++
              alloc (sizeof e.expr_typ) ++
              popq rbx ++
              pushq (reg rax) ++
              call ("copy_" ^ s.s_name) (* on passe une copie de la structure *)
          | _ -> pushq (reg rdi) (* on met l'argement sur la pile *)
      ) el nop ++
      call ("F_" ^ f.fn_name) ++
      leaq (ind ~ofs:(8 * List.length el) rsp) rsp
  
  (* Compilation champs de structure *)
  | TEdot (e1, {f_ofs=ofs}) ->
      expr env e1 ++
      leaq (ind ~ofs:ofs rsi) rsi ++
      movq (ind rsi) (reg rdi)

  (* Cas exclus par le bloc *)
  | TEvars _ ->
      assert false

  (* Compilation return *)
  | TEreturn [] ->
      movq (reg rbp) (reg rsp) ++
      jmp env.exit_label
  | TEreturn [e1] ->
      expr env e1 ++
      movq (reg rbp) (reg rsp) ++
      jmp env.exit_label
  (* Cas exclus par rewrite *)
  | TEreturn _ ->
      assert false

  (* Compilation incrementation decremenetation*)
  | TEincdec (e1, op) ->
      expr env e1 ++
      (match op with | Inc -> incq | Dec -> decq) (ind rsi)
  ) ++ comment_expr e ">"



let decl code = function
    (* Compilation des fonctions *)
  | TDfunction (f, e) ->
      ignore (List.fold_left (fun ofs v -> v.v_ofs <- ofs; ofs + sizeof v.v_typ) (8*2) f.fn_params); (* calcul des offsets des arguments par rapport au futur rbp *)
      code ++
      let s = f.fn_name and exitl = new_label () in
      label ("F_" ^ s) ++
      pushq (reg rbp) ++
      movq (reg rsp) (reg rbp) ++
      expr { exit_label = exitl; next_local_ofs = -8 } e ++ (* le coprs de la fonction *)
      label exitl ++
      popq rbp ++
      ret

  (* Compilation des structures (compilation des fonctions d'affichage, egalite, copie associees)*)
  | TDstruct s ->
      ignore (sizeof (Tstruct s)); (* on calcule le decalage des champs *)
      let fl = List.of_seq (Hashtbl.to_seq_values s.s_fields) in
      code ++

      (* Test d'egalite pour la structure *)
      label ("eq_" ^ s.s_name) ++
      movq (imm 1) (reg rdi) ++
      List.fold_left (fun code f ->
        pushq (reg rax) ++
        pushq (reg rbx) ++
        pushq (reg rdi) ++
        leaq (ind ~ofs:f.f_ofs rax) rax ++
        leaq (ind ~ofs:f.f_ofs rbx) rbx ++
        (match f.f_typ with
          | Tint | Tptr _ | Tnil | Tbool ->
              movq (ind rax) (reg rax) ++
              movq (ind rbx) (reg rbx) ++
              cmpq (reg rax) (reg rbx) ++
              sete (reg dil)
          | Tstring -> call "eq_string"
          | Tstruct s -> call ("eq_" ^ s.s_name) (* on teste recursivement l'egalite *)
          | _ -> assert false) ++
        popq r8 ++
        popq rbx ++
        popq rax ++
        andb (reg r8b) (reg dil) ++
        code
      ) ret fl ++

      (* Copie de la structure *)
      label ("copy_" ^ s.s_name) ++
      List.fold_left (fun code f ->
        pushq (reg rax) ++
        pushq (reg rbx) ++
        leaq (ind ~ofs:f.f_ofs rax) rax ++
        leaq (ind ~ofs:f.f_ofs rbx) rbx ++
        (match f.f_typ with
          | Tint | Tptr _ | Tnil | Tbool ->
              movq (ind rbx) (reg rdx) ++
              movq (reg rdx) (ind rax)
          | Tstring -> call "copy_string"
          | Tstruct s -> call ("copy_" ^ s.s_name) (* copie recursive *)
          | _ -> assert false) ++
        popq rbx ++
        popq rax ++
        code
      ) ret fl ++

      (* Affichage de la structure *)
      label ("print_struct_" ^ s.s_name) ++
      pushq (reg rsi) ++
      movq (ilab "S_openb") (reg rdi) ++
      call "print_string" ++
      popq rsi ++
      let sorted_f = List.sort (fun a b -> a.f_print_order - b.f_print_order) (List.of_seq (Hashtbl.to_seq_values s.s_fields)) in (* on remet les champs dans l'ordre de leur declaration *)
      List.fold_right (fun f code ->
        pushq (reg rsi) ++
        leaq (ind ~ofs:f.f_ofs rsi) rsi ++
        movq (ind rsi) (reg rdi) ++
        (match f.f_typ with
          | Tint -> call "print_int"
          | Tbool -> call "print_bool"
          | Tstring -> call "print_string"
          | Tnil -> call "print_nil"
          (* en fait il ne faut pas afficher recursivement les pointeurs de structures ... *)
          (* | Tptr (Tstruct s) ->
              let lnil = new_label () and l = new_label () in
              testq (reg rdi) (reg rdi) ++
              jz lnil ++
              movq (reg rdi) (reg rsi) ++
              pushq (reg rsi) ++
              movq (ilab "S_amp") (reg rdi) ++
              call "print_string" ++
              popq rsi ++
              call ("print_struct_" ^ s.s_name) ++
              jmp l ++
              label lnil ++
              call "print_nil" ++
              label l *)
          | Tptr _ -> call "print_pointer"
          | Tstruct s -> call ("print_struct_" ^ s.s_name)
          | _ -> assert false) ++
        (if code <> nop then call "print_space"
        else nop) ++
        popq rsi ++
        code
      ) sorted_f nop ++
      movq (ilab "S_closeb") (reg rdi) ++
      call "print_string" ++
      ret

let file ?debug:(b=false) dl =
  debug := b;
  let funs = List.fold_left decl nop dl in
  { text =
      globl "main" ++ label "main" ++
      call "F_main" ++
      xorq (reg rax) (reg rax) ++
      ret ++
      funs ++
      inline "
print_int:
      movq %rdi, %rsi
      movq $S_int, %rdi
      jmp _printf
print_space:
      movq $S_space, %rdi
      jmp _printf
print_bool:
      movq %rdi, %rax
      movq $S_true, %rdi
      testq %rax, %rax
      jnz _printf
      movq $S_false, %rdi
      jmp _printf
print_nil:
      movq $S_nil, %rdi
      jmp _printf
print_pointer:
      test %rdi, %rdi
      jz print_nil
      movq %rdi, %rsi
      movq $S_pointer, %rdi
      jmp _printf
print_string:
_printf:
      xorq %rax, %rax 
      call printf
      ret
eq_string:
      xorq %rdi, %rdi
      xorq %r8, %r8
      jmp _eq_string_test
_eq_string_loop:
      incq %rax
      incq %rbx
_eq_string_test:
      movb (%rax), %cl
      movb (%rbx), %dl
      cmpb %cl, %dl
      sete %dil
      testb %cl, %cl
      setne %r8b
      testb %dil, %r8b
      jnz _eq_string_loop
      ret
";
    data =
      label "S_int" ++ string "%ld" ++
      label "S_space" ++ string " " ++
      label "S_true" ++ string "true" ++
      label "S_false" ++ string "false" ++
      label "S_nil" ++ string "<nil>" ++
      label "S_amp" ++ string "&" ++
      label "S_openb" ++ string "{" ++
      label "S_closeb" ++ string "}" ++
      label "S_pointer" ++ string "%p" ++
      (Hashtbl.fold (fun l s d -> label l ++ string s ++ d) strings nop)
    ;
  }
