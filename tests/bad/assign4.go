package main

type TT struct { a int }
type T struct { tt TT }

func bar() T {
	return *new(T)
}

func main() {
	bar().tt = *new(TT)
}
