package main
import "fmt"

func foo() (bool, bool) { return true, false; }
func bar() int { return 0; }

func main() {
	fmt.Print(bar(), foo())
}
