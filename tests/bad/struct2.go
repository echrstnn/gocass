package main

type T struct { a int }

func foo() T { return *new(T) }

func main() {
	foo().a = 3
}
