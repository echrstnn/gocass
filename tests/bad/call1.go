package main

func foo(a int, b int, c bool) {}
func bar() (bool, int, int) { return true, 1, 2 }

func main() {
	foo(bar())
}
