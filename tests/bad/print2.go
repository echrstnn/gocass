package main
import "fmt"

func foo() {}
func bar() int { return 0; }

func main() {
	fmt.Print(bar(), foo())
}
