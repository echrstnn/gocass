package main

type TT struct { a int }
type T struct { tt TT }

func foo() (bool, bool) { return true, false; }
func bar() int { return 0; }

func main() {
	var x, y bool
	var z int
	x, y, z = foo(), bar()
}
