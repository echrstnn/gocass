package main

type TT struct { a int }
type T struct { tt TT }

func foo() (bool, bool) { return true, false; }
func bar() int { return 0; }

func main() {
	var x int
	var y T
	x, y = x, y.tt
}
