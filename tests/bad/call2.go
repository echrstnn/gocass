package main

func zer() (bool, int) {return true, 1}
func foo(a bool, b int) (int, bool) { return b, a }
func bar(a bool, b int) (bool, int) { return a, b }

func main() {
	foo(foo(bar(zer())))
}
