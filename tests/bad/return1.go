package main

func foo() (int, int, int, int) { return bar(), bar() }
func bar() (int, int) { return 0, 1 }

func main() {
}
