package main

type TT struct { a int }
type T struct { tt TT }

func bar() (*T, *T) {
	return new(T), new(T)
}

func main() {
	var t T
	bar() = &t, &t
}
