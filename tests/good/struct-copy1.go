package main
import "fmt"

type Foo struct { v int }

func foo(v int) Foo {
	var ans Foo
	ans.v = v
	return ans
}

func print(a, b Foo) {
	fmt.Print("a ", a.v, "  b ", b.v)
	if a == b {
		fmt.Print(" ==\n")
	} else {
		fmt.Print(" !=\n")
	}
}

func bar(a Foo) (Foo, Foo) { return a, a }

func main() {
	var a, b = foo(0), foo(1)
	print(a, b)
	a, b = bar(foo(42))
	print(bar(foo(42)))
	print(a, b)
	b.v = 2
	print(a, b)
}
