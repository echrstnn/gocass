package main
import "fmt"

func foo(x int) (int, bool) {
	return x, true
}

func main() {
	var x int
	var y bool
	x, y = foo(20)
	fmt.Print(x, y, "\n");
}
