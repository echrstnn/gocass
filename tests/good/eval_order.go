package main
import "fmt"

type T struct { a,b int }

func foo() *int { fmt.Print("foo"); return new(int) }
func bar() int { fmt.Print("bar"); return 0; }

func main() {
	*foo() = bar()
}
