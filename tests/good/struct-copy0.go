package main
import "fmt"

type Foo struct { v int }

func foo(v int) Foo {
	var ans Foo
	ans.v = v
	return ans
}

func print(a, b Foo) {
	fmt.Print("a ", a.v, "  b ", b.v, "\n")
}

func main() {
	var a, b = foo(0), foo(1)
	print(a, b)
	a = b
	print(a, b)
	b.v = 2
	print(a, b)
}
